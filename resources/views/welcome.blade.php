@extends('app')

@section('content')
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    @foreach ($slides as $slide)
                        <div class="
                            item
                            @if ($slides->first()->id == $slide->id)
                                active
                            @endif
                        ">
                            <img src="{{ $slide->image_public_src }}" />
                            <div class="carousel-caption">
                                Third Slide - new slide {{ $slide->id }}
                            </div>
                        </div>
                    @endforeach

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

            <div class="row specials">
                <div class="col-md-4">
                    <div class="product center-block">
                        <img src="images/sample-product.png" class="img-responsive img-thumbnail" />
                        <h3 class="name">
                            Product 1
                        </h3>
                        <h4 class="price">
                            $20.00
                        </h4>
                        <div class="buttons">
                            <button type="button" class="btn btn-success pull-right">Add to Cart!</button>
                            <button type="button" class="btn btn-primary pull-left">Add to Wish List!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product center-block">
                        <img src="images/sample-product.png" class="img-responsive img-thumbnail" />
                        <h3 class="name">
                            Product 1
                        </h3>
                        <h4 class="price">
                            $20.00
                        </h4>
                        <div class="buttons">
                            <button type="button" class="btn btn-success pull-right">
                                Add to Cart!
                            </button>
                            <button type="button" class="btn btn-primary pull-left">Add to Wish List!</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="product center-block">
                        <img src="images/sample-product.png" class="img-responsive img-thumbnail" />
                        <h3 class="name">
                            Product 1
                        </h3>
                        <h4 class="price">
                            $20.00
                        </h4>
                        <div class="buttons">
                            <button type="button" class="btn btn-success pull-right">Add to Cart!</button>
                            <button type="button" class="btn btn-primary pull-left">Add to Wish List!</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ads">
                <div class="col-md-6">
                    <img src="images/facebook.png" class="img-responsive center-block" />
                </div>
                <div class="col-md-6">
                    <img src="images/freeshipping.png" class="img-responsive center-block" />
                </div>
            </div>

        @endsection