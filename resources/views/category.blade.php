@extends('app')

@section('content')
<div class="container">
    <h2>{{ $category->category }}</h2>

    <div class="row specials">
        @foreach ($category->products()->get() as $product) 
            <div class="col-md-4">
                <div class="product center-block">
                    <a href="{{ route('product_view', ['id' => $product->id]) }}">
                        <img src="{{ url('images/sample-product.png') }}" class="img-responsive img-thumbnail" />
                        <h3 class="name">
                            {{ $product->name }}
                        </h3>
                    </a>
                    <h4 class="price">
                        ${{ $product->price }}
                    </h4>
                    <div class="buttons">
                        <button type="button" class="btn btn-success center-block">Add to Cart!</button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection
