@extends('app')

@section('content')
<div class="container" style="width: 80%">
    <h2>My Cart</h2>

    @if (count($items) == 0)
        There is no items in your cart.
    @else
        @foreach ($items as $item)
            <div>
                <span>{{ $item['product']->name }}</span>
                <span>{{ $item['qty'] }}</span>
            </div>
        @endforeach 
    @endif

    <a href="{{ route('checkout') }}" class="btn btn-success pull-right">
        Checkout
    </a> 
</div>
@endsection
