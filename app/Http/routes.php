<?php

use \App\Category;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', array('uses' => 'WelcomeController@index', 'as' => 'homepage'));

Route::get('category/{id}', array('uses' => 'Categories@view', 'as' => 'category_view'));

Route::get('product/{id}', array('uses' => 'ProductController@index', 'as' => 'product_view'));

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

View::share('categories', Category::where('level', '=', '0')->get());

Route::get('cart', array(
    'uses' => 'CartController@view',
    'as'   => 'cart_view',
));

Route::post('cart/add', array(
    'uses' => 'CartController@addToCart',
    'as'   => 'cart_add',
));

Route::get('checkout', array(
    'uses' => 'CheckoutController@index',
    'as'   => 'checkout',
));

Route::post('checkout', array(
    'uses' => 'CheckoutController@doCheckout',
    'as'   => 'checkout.post',
));