<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use App\Order;
use App\Cart;
use App\Product;

class CheckoutController extends Controller {

	public function index()
    {
        return view('checkout');
    }

    public function doCheckout()
    {
        $items = Cart::getAllItems();

        $price = 0;
        foreach ($items as $key => $item) {
            $product = Product::find($item['product_id']);

            $price += $item['qty'] * $product->price;
        }

        $order = Order::create([]);

        var_dump($order->charge($price * 100, ['source' => Request::input('stripeToken')]));
    }
}
