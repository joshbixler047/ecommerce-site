<?php
/**
 * Users model config
 */
return array(
    'title' => 'Users',
    'single' => 'User',
    'model' => '\\App\\User',
    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'name',
        'email',
        'password',
        'admin',
    ),
    /**
     * The filter set
     */
    'filters' => array(
        'id',
        'name',
        'email',
    ),
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'name',
        'email',
        'password' => array(
            'type' => 'password',
            'title' => 'Password',
        ),
        'admin' => array(
            'type'  => 'bool',
            'title' => 'Is Admin',
        ),
    ),
);