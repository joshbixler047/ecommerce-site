<?php
use \App\Slide;

/**
 * Slides model config
 */
return array(
    'title' => 'Slides',
    'single' => 'Slide',
    'model' => '\\App\\Slide',
    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'image_public_src' => array(
            'title'  => 'Image',
            'output' => '<img src="(:value)" height="100" />',
        ),
        'link',
    ),
    /**
     * The filter set
     */
    'filters' => array(
        'id',
        'link',
    ),
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'link',
        'image' => array(
            'title' => 'Image',
            'type' => 'image',
            'location' => public_path() . '/' . Slide::getImagePath(),
            'naming' => 'random',
            'length' => 20,
            'size_limit' => 2,
        ),
        'enable' => array(
            'title' => 'Enabled?',
            'type'  => 'bool',
        ),
    ),
);